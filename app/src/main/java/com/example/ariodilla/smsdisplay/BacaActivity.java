package com.example.ariodilla.smsdisplay;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ariodilla.smsdisplay.model.DataSMS;
import com.example.ariodilla.smsdisplay.proses.AmbilSMS;
import com.example.ariodilla.smsdisplay.proses.BacaTulisFile;
import com.example.ariodilla.smsdisplay.proses.EnkripsiAES;
import com.example.ariodilla.smsdisplay.proses.KirimTerimaSMS;

import java.util.ArrayList;

public class BacaActivity extends AppCompatActivity {

    Toolbar toolbar_baca;
    TextView tv_pesan, tv_balas;
    EditText txt_kunci, txt_pesan;
    Button btn_buka, btn_kirim;
    DataSMS dataSMS;
    EnkripsiAES AES;
    KirimTerimaSMS SMS;
    String kunci;
    BacaTulisFile FILE = new BacaTulisFile();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baca);

        toolbar_baca = (Toolbar)findViewById(R.id.toolbar_baca);
        tv_pesan = (TextView)findViewById(R.id.tv_isipesan);
        tv_balas = (TextView)findViewById(R.id.tv_balas);
        txt_kunci = (EditText)findViewById(R.id.txt_kunci);
        txt_pesan = (EditText)findViewById(R.id.txt_pesan);
        btn_buka = (Button)findViewById(R.id.btn_buka);
        btn_kirim = (Button)findViewById(R.id.btn_kirim);

        Intent intent = getIntent();
        dataSMS = intent.getParcelableExtra("Data");
        String sinyal = intent.getStringExtra("Sinyal");


        kunci = FILE.readFromFile(BacaActivity.this);
        if(sinyal.equals("outbox")){
            tv_balas.setVisibility(View.GONE);
            txt_pesan.setVisibility(View.GONE);
            btn_kirim.setVisibility(View.GONE);
            kunci = dataSMS.getNumber();

        }

        ToolbarSettings(dataSMS.getNama(),dataSMS.getNumber());

        try {
            tv_pesan.setText(AES.decryptAES(dataSMS.getPesan(), kunci));
            Toast.makeText(BacaActivity.this,"Pesan berhasil di decrypt",Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            tv_pesan.setText(dataSMS.getPesan());
            Toast.makeText(BacaActivity.this,"Pesan gagal di decrypt !\nSilahkan masukkan kunci secara manual",Toast.LENGTH_SHORT).show();
        }

        btn_buka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    tv_pesan.setText(AES.decryptAES(dataSMS.getPesan(), txt_kunci.getText().toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(BacaActivity.this,"Kunci yang anda masukkan tidak sesuai !",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!txt_pesan.getText().toString().isEmpty()){
                    String status = SMS.OperasiKiirimSMS(txt_pesan.getText().toString(),dataSMS.getNumber(),dataSMS.getNumber(),BacaActivity.this);
                    if(status.isEmpty()){
                        Intent intent = new Intent(BacaActivity .this, MainActivity.class);
                        setResult(Activity.RESULT_OK,intent);
                        finish();
                    }
                }else{
                    Toast.makeText(BacaActivity.this,"Tidak ada pesan yang dikirim !",Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private void ToolbarSettings(String nama, String nomer){
        toolbar_baca.setTitleTextColor(Color.WHITE);

        if(!nama.isEmpty()){
            toolbar_baca.setTitle(" "+nama);
        }else{
            toolbar_baca.setTitle(" "+nomer);
        }

        toolbar_baca.setLogo(R.drawable.ic_action_email);
    }
}
