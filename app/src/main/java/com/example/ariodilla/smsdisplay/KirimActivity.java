package com.example.ariodilla.smsdisplay;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.ariodilla.smsdisplay.model.DataSMS;
import com.example.ariodilla.smsdisplay.proses.AmbilSMS;
import com.example.ariodilla.smsdisplay.proses.BacaTulisFile;
import com.example.ariodilla.smsdisplay.proses.EnkripsiAES;
import com.example.ariodilla.smsdisplay.proses.KirimTerimaSMS;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class KirimActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText txt_number, txt_pesan;
    ImageButton btn_cari;
    Button btn_kirim;
    KirimTerimaSMS kirimTerimaSMS = new KirimTerimaSMS();
    BacaTulisFile FILE = new BacaTulisFile();

    private static final int PICK_CONTACT = 1;

    private Uri uriContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kirim);

        toolbar = (Toolbar)findViewById(R.id.toolbar_kirim);
        txt_number = (EditText)findViewById(R.id.txtnumber);
        txt_pesan = (EditText)findViewById(R.id.txtpesan);
        btn_cari = (ImageButton)findViewById(R.id.btncari);
        btn_kirim = (Button)findViewById(R.id.btnkirim);

//        final ArrayList<DataSMS> smskeluar = getIntent().getParcelableArrayListExtra("smskeluar");
//        final ArrayList<DataSMS> smsmasuk = getIntent().getParcelableArrayListExtra("smsmasuk");

        ToolbarSettings();

        btn_cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });

        btn_kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String status = kirimTerimaSMS.OperasiKiirimSMS(
                        txt_pesan.getText().toString(),
                        txt_number.getText().toString(),
                        txt_number.getText().toString(),
                        KirimActivity.this
                );

                if(status.isEmpty()){
                    Intent intent = new Intent(KirimActivity.this, MainActivity.class);
                    setResult(Activity.RESULT_OK,intent);
                    finish();
                }else{
                    Toast.makeText(KirimActivity.this,status,Toast.LENGTH_LONG).show();
                }



            }
        });

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_CONTACT && resultCode == RESULT_OK) {
            uriContact = data.getData();
            AmbilTelepon();
        }
    }


    private void AmbilTelepon() {
        String contactNumber = null;
//        String contactName = null;

        Cursor cursor = getContentResolver().query(uriContact, null, null, null, null);
        if (cursor.moveToFirst()) {
            int phoneNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int phoneName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            contactNumber = cursor.getString(phoneNumber);
//            contactName = cursor.getString(phoneName);
        }

        cursor.close();

        txt_number.setText(contactNumber);
    }

    public void ToolbarSettings(){
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle(" Kirim SMS");
        toolbar.setLogo(R.drawable.ic_send_white);
    }
}
