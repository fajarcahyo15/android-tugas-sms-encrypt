package com.example.ariodilla.smsdisplay;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ariodilla.smsdisplay.adapter.TabAdapter;
import com.example.ariodilla.smsdisplay.model.DataSMS;
import com.example.ariodilla.smsdisplay.proses.AmbilSMS;
import com.example.ariodilla.smsdisplay.proses.BacaTulisFile;
import com.example.ariodilla.smsdisplay.proses.DialogBox;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private TabLayout mSlidingTabLayout;
    private ViewPager mViewPager;
    private ArrayList<DataSMS> smsmasuk, smskeluar;
    private BacaTulisFile FILE = new BacaTulisFile();
    private DialogBox Dialog = new DialogBox(MainActivity.this);
    private int halaman;
    private BroadcastReceiver sendBroadcastReceiver;
    private BroadcastReceiver deliveryBroadcastReceiver;
    String SENT = "SMS_SENT";
    String DELIVERED = "SMS_DELIVERED";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Broadcast();

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setLogo(R.mipmap.ic_security_red);


        mViewPager=(ViewPager)findViewById(R.id.vp_tabs);
        mSlidingTabLayout=(TabLayout) findViewById(R.id.tl1);

        smskeluar = (ArrayList<DataSMS>) getIntent().getSerializableExtra("smskeluar");
        smsmasuk = (ArrayList<DataSMS>) getIntent().getSerializableExtra("smsmasuk");

        if(FILE.readFromFile(MainActivity.this).isEmpty()){
            Dialog.inputKunci(0);
        }


        BacaSMS(smskeluar,smsmasuk);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();

        unregisterReceiver(sendBroadcastReceiver);
        unregisterReceiver(deliveryBroadcastReceiver);
    }


    @Override
    protected void onResume() {
        super.onResume();
        Broadcast();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_konfig)
        {
            Intent intent = new Intent(MainActivity.this,SettingActivity.class);
            startActivity(intent);
        }else if (id == R.id.action_tambah)
        {
            //Toast.makeText(this,"Kirim Pesan",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(MainActivity.this, KirimActivity.class);
            intent.putParcelableArrayListExtra("smskeluar",smskeluar);
            intent.putParcelableArrayListExtra("smsmasuk",smsmasuk);
            startActivityForResult(intent,1);

        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==1){
            if(resultCode == Activity.RESULT_OK){
                BacaSMS(smskeluar,smsmasuk);
                mViewPager.setCurrentItem(2);
            }
        }
    }


    private void Broadcast(){
        sendBroadcastReceiver = new BroadcastReceiver()
        {

            public void onReceive(Context arg0, Intent arg1)
            {
                switch(getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(),"SMS Terkirim",Toast.LENGTH_LONG).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(),"Error",Toast.LENGTH_LONG).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(),"Tidak ada kartu SIM",Toast.LENGTH_LONG).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(),"Error",Toast.LENGTH_LONG).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(),"Tidak ada sinyal",Toast.LENGTH_LONG).show();
                        break;
                }
            }
        };

        deliveryBroadcastReceiver = new BroadcastReceiver()
        {
            public void onReceive(Context arg0, Intent arg1)
            {
                switch(getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(),"SMS Telah diterima",Toast.LENGTH_LONG).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(),"Batal mengirim SMS",Toast.LENGTH_LONG).show();
                        break;
                }
            }
        };
        registerReceiver(deliveryBroadcastReceiver, new IntentFilter(DELIVERED));
        registerReceiver(sendBroadcastReceiver , new IntentFilter(SENT));
    }


    private void BacaSMS(ArrayList<DataSMS> smskeluar, ArrayList<DataSMS> smsmasuk){
        mViewPager.setAdapter(new TabAdapter(getSupportFragmentManager(),getApplicationContext(),smskeluar,smsmasuk));
        mSlidingTabLayout.setupWithViewPager(mViewPager);

        mSlidingTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }
        });

    }




}
