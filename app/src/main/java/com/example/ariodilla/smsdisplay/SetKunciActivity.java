package com.example.ariodilla.smsdisplay;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ariodilla.smsdisplay.proses.BacaTulisFile;

public class SetKunciActivity extends AppCompatActivity {

    Toolbar toolbat_kunci;
    TextView tv_kunci;
    EditText txt_kunci;
    Button btn_simpan;
    BacaTulisFile FILE = new BacaTulisFile();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_kunci);

        toolbat_kunci = (Toolbar)findViewById(R.id.toolbar_kunci);
        tv_kunci = (TextView)findViewById(R.id.tv_kunci);
        txt_kunci = (EditText)findViewById(R.id.txt_kunci);
        btn_simpan = (Button)findViewById(R.id.btn_set_kunci);

        ToolbarSettings();

        tv_kunci.setText(FILE.readFromFile(SetKunciActivity.this));

        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FILE.writeToFile(txt_kunci.getText().toString(),SetKunciActivity.this);
            }
        });


    }

    private void ToolbarSettings(){
        toolbat_kunci.setTitleTextColor(Color.WHITE);
        toolbat_kunci.setTitle(" Set Kunci");
        toolbat_kunci.setLogo(R.drawable.ic_key_white);
    }
}
