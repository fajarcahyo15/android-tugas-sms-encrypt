package com.example.ariodilla.smsdisplay;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ariodilla.smsdisplay.adapter.SettingListAdapter;
import com.example.ariodilla.smsdisplay.model.DataSetting;
import com.example.ariodilla.smsdisplay.proses.BacaTulisFile;
import com.example.ariodilla.smsdisplay.proses.DialogBox;

import java.util.ArrayList;
import java.util.Set;

public class SettingActivity extends AppCompatActivity {

    ListView lv_setting;
    Toolbar toolbar_setting;
    ArrayList<DataSetting> menu = new ArrayList<>();
    DataSetting dataSetting = new DataSetting();
    BacaTulisFile FILE = new BacaTulisFile();
    DialogBox Dialog = new DialogBox(SettingActivity.this);
    SettingListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        lv_setting = (ListView)findViewById(R.id.lv_setting);
        toolbar_setting = (Toolbar)findViewById(R.id.toolbar_setting);

        ToolbarSettings();

        adapter = new SettingListAdapter(SettingActivity.this,R.layout.setting_list,menu());
        adapter.notifyDataSetChanged();
        lv_setting.setAdapter(adapter);

        lv_setting.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent intent = new Intent(SettingActivity.this, SetKunciActivity.class);
//                startActivity(intent);
                Dialog.inputKunci(1);

            }
        });
    }

    private void ToolbarSettings(){
        toolbar_setting.setTitleTextColor(Color.WHITE);
        toolbar_setting.setTitle(" Settings");
        toolbar_setting.setLogo(R.drawable.ic_setting_white);
    }

    private ArrayList<DataSetting> menu(){

        dataSetting.setJudul("Set no telepon");
        dataSetting.setKeterangan("No telepon saat ini : "+FILE.readFromFile(SettingActivity.this));
        menu.add(dataSetting);

        return menu;
    }




}
