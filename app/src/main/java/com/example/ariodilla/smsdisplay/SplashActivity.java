package com.example.ariodilla.smsdisplay;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.ariodilla.smsdisplay.model.DataSMS;
import com.example.ariodilla.smsdisplay.proses.AmbilSMS;

import java.util.ArrayList;

public class SplashActivity extends AppCompatActivity{


    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSION = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        TextView tv = (TextView) findViewById(R.id.Judul);
        Typeface face = Typeface.createFromAsset(getAssets(),
                "dq.ttf");
        tv.setTypeface(face);

//        if (ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this, Manifest.permission.READ_SMS)) {
//            } else {
//                ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.READ_SMS}, MY_PERMISSIONS_REQUEST_READ_SMS);
//            }
//        }
        permissionForMarshmallow();



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {



        switch (requestCode) {

            case REQUEST_CODE_ASK_MULTIPLE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new getSMS().execute();
                } else {

                    // Toast.makeText(LoginActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    private class getSMS extends AsyncTask<Object, Object, Void> {

        ArrayList<DataSMS> SMSKeluar = new ArrayList<>();
        ArrayList<DataSMS> SMSMasuk = new ArrayList<>();

        @Override
        protected Void doInBackground(Object... params) {



            AmbilSMS ambilSMS = new AmbilSMS();

            SMSKeluar = ambilSMS.BacaSMSTerkirim(SplashActivity.this);
            SMSMasuk = ambilSMS.BacaSMSMasuk(SplashActivity.this);

            return  null;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Intent intent = new Intent(SplashActivity.this,MainActivity.class);
            intent.putExtra("smskeluar", SMSKeluar);
            intent.putExtra("smsmasuk", SMSMasuk);
            startActivity(intent);
        }
    }

    private void permissionForMarshmallow() {

        int permissionReadSMSCheck = ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.READ_SMS);
        int permissionReadContacsCheck = ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.READ_CONTACTS);
        int permissionSendSMSCheck = ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.SEND_SMS);

        if((permissionReadSMSCheck != PackageManager.PERMISSION_GRANTED)||(permissionReadContacsCheck != PackageManager.PERMISSION_GRANTED)||(permissionSendSMSCheck != PackageManager.PERMISSION_GRANTED)){
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.READ_SMS,Manifest.permission.READ_CONTACTS,Manifest.permission.SEND_SMS}, REQUEST_CODE_ASK_MULTIPLE_PERMISSION);
        }

        if((permissionReadSMSCheck==PackageManager.PERMISSION_GRANTED)&&
                (permissionReadContacsCheck==PackageManager.PERMISSION_GRANTED)&&
                (permissionSendSMSCheck==PackageManager.PERMISSION_GRANTED)){
            new getSMS().execute();
        }
    }
}



