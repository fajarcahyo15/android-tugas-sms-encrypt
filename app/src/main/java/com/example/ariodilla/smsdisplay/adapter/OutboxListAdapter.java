package com.example.ariodilla.smsdisplay.adapter;

import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ariodilla.smsdisplay.R;
import com.example.ariodilla.smsdisplay.model.DataSMS;

import java.lang.reflect.Array;
import java.util.List;

/**
 * Created by Fajar Cahyo Prabowo on 07-Jun-17.
 */

public class OutboxListAdapter extends ArrayAdapter<DataSMS> {



    public OutboxListAdapter(Context context, int resource, List<DataSMS> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.outbox_list, null);
        }

        DataSMS p = getItem(position);

        if (p != null) {
            TextView txt_dari = (TextView) v.findViewById(R.id.txt_dari);
            TextView txt_pesan = (TextView) v.findViewById(R.id.txt_pesan);
            TextView txt_waktu = (TextView) v.findViewById(R.id.txt_tanggal);

            if (txt_dari != null) {
                if(p.getNama().isEmpty()){
                    txt_dari.setText("Ke : "+p.getNumber());
                }else{
                    if(p.getNama().isEmpty()){
                        txt_dari.setText("Ke : "+p.getNumber());
                    }
                    else {
                        txt_dari.setText("Ke : "+p.getNama());
                    }

                }

            }

            if (txt_pesan != null) {
                txt_pesan.setText(p.getPesan());
            }

            if (txt_waktu != null) {
                txt_waktu.setText(p.getTanggal());
            }
        }

        return v;
    }
}
