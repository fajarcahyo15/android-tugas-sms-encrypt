package com.example.ariodilla.smsdisplay.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ariodilla.smsdisplay.R;
import com.example.ariodilla.smsdisplay.model.DataSetting;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fajar Cahyo Prabowo on 11-Jun-17.
 */

public class SettingListAdapter extends ArrayAdapter<DataSetting> {

    public SettingListAdapter(Context context,int resource, List<DataSetting> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.setting_list, null);
        }



        DataSetting p = getItem(position);


        if (p != null) {
            TextView txt_judul = (TextView)v.findViewById(R.id.tv_setting_judul);
            TextView txt_keterangan = (TextView)v.findViewById(R.id.tv_setting_ket);

            txt_judul.setText(p.getJudul());
            txt_keterangan.setText(p.getKeterangan());

        }

        return v;
    }




}
