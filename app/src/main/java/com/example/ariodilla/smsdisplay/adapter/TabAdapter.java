package com.example.ariodilla.smsdisplay.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.example.ariodilla.smsdisplay.fragment.InboxFragment;
import com.example.ariodilla.smsdisplay.fragment.OutboxFragment;
import com.example.ariodilla.smsdisplay.model.DataSMS;

import java.util.ArrayList;

/**
 * Created by Fajar Cahyo Prabowo on 01/06/2017.
 */

public class TabAdapter extends FragmentPagerAdapter {
    String fragments [] = {"PESAN MASUK","PESAN TERKIRIM"};
    Context context;
    ArrayList<DataSMS> dataOutbox;
    ArrayList<DataSMS> dataInbox;

    public TabAdapter(FragmentManager supportFragmentManager, Context applicationContext, ArrayList<DataSMS> dtoutbox, ArrayList<DataSMS> dtinbox ) {
        super(supportFragmentManager);
        this.context = applicationContext;
        this.dataOutbox = dtoutbox;
        this.dataInbox = dtinbox;


    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {


        if(position ==0){

            return new InboxFragment(dataInbox);
        }else if(position == 1){

            return new OutboxFragment(dataOutbox);
        }else{
            return null;
        }

    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragments[position];
    }
}
