package com.example.ariodilla.smsdisplay.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ariodilla.smsdisplay.BacaActivity;
import com.example.ariodilla.smsdisplay.R;
import com.example.ariodilla.smsdisplay.adapter.InboxListAdapter;
import com.example.ariodilla.smsdisplay.model.DataSMS;
import com.example.ariodilla.smsdisplay.proses.AmbilSMS;

import java.util.ArrayList;


public class InboxFragment extends Fragment {

    ArrayList<DataSMS> SMSData;
    ListView lv_inbox;
    TextView ket;
    AmbilSMS ambilSMS = new AmbilSMS();
    SwipeRefreshLayout mSwipeRefreshLayout;

    public InboxFragment(ArrayList<DataSMS> dataSMSes) {
        // Required empty public constructor
        this.SMSData = dataSMSes;
    }



    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inbox, container, false);

        ket = (TextView)view.findViewById(R.id.keterangan);
        ket.setVisibility(View.GONE);

        lv_inbox = (ListView)view.findViewById(R.id.lv_lihat);
        mSwipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe);

        proses();

        lv_inbox.addHeaderView(new View(getActivity()));
        lv_inbox.addFooterView(new View(getActivity()));

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        SMSData = ambilSMS.BacaSMSMasuk(getActivity());
                    }
                }).start();

                proses();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });



        return view;
    }

    private void proses(){

        if(SMSData==null){
            ket.setVisibility(View.VISIBLE);
            lv_inbox.setVisibility(View.GONE);
        }else{
            InboxListAdapter adapter = new InboxListAdapter(getActivity(),R.layout.inbox_list, SMSData);
            adapter.notifyDataSetChanged();
            lv_inbox.setAdapter(adapter);

            lv_inbox.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getActivity(), BacaActivity.class);
                    intent.putExtra("Data",SMSData.get(position-1));
                    intent.putExtra("Sinyal","inbox");
                    startActivityForResult(intent,1);

                }
            });

        }
    }
}
