package com.example.ariodilla.smsdisplay.fragment;



import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ariodilla.smsdisplay.BacaActivity;
import com.example.ariodilla.smsdisplay.R;
import com.example.ariodilla.smsdisplay.adapter.OutboxListAdapter;
import com.example.ariodilla.smsdisplay.model.DataSMS;
import com.example.ariodilla.smsdisplay.proses.AmbilSMS;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ariodilla on 5/31/2017.
 */

public class OutboxFragment extends Fragment {

    ArrayList<DataSMS> SMSData;
    ListView lv_inbox;
    TextView ket;
    SwipeRefreshLayout mSwipeRefreshLayout;
    AmbilSMS ambilSMS = new AmbilSMS();

    public OutboxFragment(ArrayList<DataSMS> dataSMSes) {
        // Required empty public constructor
        this.SMSData = dataSMSes;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_outbox, container, false);

        lv_inbox = (ListView)view.findViewById(R.id.lv_lihat);
        mSwipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe);
        ket = (TextView)view.findViewById(R.id.keterangan);
        ket.setVisibility(View.GONE);

        proses();

        lv_inbox.addHeaderView(new View(getActivity()));
        lv_inbox.addFooterView(new View(getActivity()));

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                //membuat thread baru untuk mengambil data (Asynchronous)
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        SMSData = ambilSMS.BacaSMSTerkirim(getActivity());
                    }
                }).start();

                proses();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        return view;
    }

    private void proses(){

        if(SMSData== null){
            ket.setVisibility(View.VISIBLE);
            lv_inbox.setVisibility(View.GONE);
        }else{

            OutboxListAdapter adapter = new OutboxListAdapter(getActivity(),R.layout.outbox_list, SMSData);
            adapter.notifyDataSetChanged();
            lv_inbox.setAdapter(adapter);

            lv_inbox.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getActivity(), BacaActivity.class);
                    intent.putExtra("Data",SMSData.get(position-1));
                    intent.putExtra("Sinyal","outbox");
                    startActivity(intent);
                }


            });
        }
    }
}
