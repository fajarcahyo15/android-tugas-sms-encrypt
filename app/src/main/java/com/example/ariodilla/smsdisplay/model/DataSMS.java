package com.example.ariodilla.smsdisplay.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Fajar Cahyo Prabowo on 07-Jun-17.
 */

public class DataSMS implements Parcelable {
    String nama;
    String number;
    String pesan;
    String tanggal;

    public DataSMS() {

    }

    protected DataSMS(Parcel in) {
        nama = in.readString();
        number = in.readString();
        pesan = in.readString();
        tanggal = in.readString();
    }

    public static final Creator<DataSMS> CREATOR = new Creator<DataSMS>() {
        @Override
        public DataSMS createFromParcel(Parcel in) {
            return new DataSMS(in);
        }

        @Override
        public DataSMS[] newArray(int size) {
            return new DataSMS[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nama);
        dest.writeString(number);
        dest.writeString(pesan);
        dest.writeString(tanggal);
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
