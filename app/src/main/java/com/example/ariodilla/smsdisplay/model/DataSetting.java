package com.example.ariodilla.smsdisplay.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Fajar Cahyo Prabowo on 11-Jun-17.
 */

public class DataSetting implements Parcelable {
    String judul;
    String keterangan;

    public DataSetting(Parcel in) {
        judul = in.readString();
        keterangan = in.readString();
    }

    public static final Creator<DataSetting> CREATOR = new Creator<DataSetting>() {
        @Override
        public DataSetting createFromParcel(Parcel in) {
            return new DataSetting(in);
        }

        @Override
        public DataSetting[] newArray(int size) {
            return new DataSetting[size];
        }
    };

    public DataSetting() {

    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(judul);
        dest.writeString(keterangan);
    }
}
