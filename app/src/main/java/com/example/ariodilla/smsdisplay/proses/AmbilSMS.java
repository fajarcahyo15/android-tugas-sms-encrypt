package com.example.ariodilla.smsdisplay.proses;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.ariodilla.smsdisplay.model.DataSMS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Fajar Cahyo Prabowo on 07-Jun-17.
 */

public class AmbilSMS {


    public ArrayList<DataSMS> BacaSMSTerkirim(Context context){


        ArrayList<DataSMS> SMSData = new ArrayList<>();

        Cursor cursor = context.getContentResolver().query(Uri.parse("content://sms/sent"), null, null, null, null);
        int total = cursor.getCount();

        if(total>100){
            total = 100;
        }

        if (cursor.moveToFirst()) { // must check the result to prevent exception
            for(int i = 0;i<total;i++){
                DataSMS dataSMS = new DataSMS();
                dataSMS.setNumber(cursor.getString(cursor.getColumnIndexOrThrow("address")));
                if(cursor.getString(2).equals(getContactName(context.getApplicationContext(),cursor.getString(2)))) {
                    dataSMS.setNama("");
                }else{
                    dataSMS.setNama(getContactName(context.getApplicationContext(),cursor.getString(2)));
                }
                dataSMS.setPesan(cursor.getString(cursor.getColumnIndexOrThrow("body")));
                dataSMS.setTanggal(parseTanggal(cursor.getString(cursor.getColumnIndexOrThrow("date"))));

                SMSData.add(dataSMS);
                cursor.moveToNext();
            }
        } else {
            SMSData = null;
        }

        return SMSData;

    }

    public ArrayList<DataSMS> BacaSMSMasuk(Context context){


        ArrayList<DataSMS> SMSData = new ArrayList<>();

        Cursor cursor = context.getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
        int total = cursor.getCount();


        if(total>100){
            total = 100;
        }

        if (cursor.moveToFirst()) { // must check the result to prevent exception
            for(int i = 0;i<total;i++){
                DataSMS dataSMS = new DataSMS();
                dataSMS.setNumber(cursor.getString(cursor.getColumnIndexOrThrow("address")));
                if(cursor.getString(2).equals(getContactName(context.getApplicationContext(),cursor.getString(2)))) {
                    dataSMS.setNama("");
                }else{
                    dataSMS.setNama(getContactName(context.getApplicationContext(),cursor.getString(2)));
                }
                dataSMS.setPesan(cursor.getString(cursor.getColumnIndexOrThrow("body")));
                dataSMS.setTanggal(parseTanggal(cursor.getString(cursor.getColumnIndexOrThrow("date"))));

                SMSData.add(dataSMS);
                cursor.moveToNext();
            }
        } else {
            SMSData = null;
        }


        return SMSData;

    }

    private String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();

        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);

        if (cursor == null) {
            return null;
        }

        String contactName = phoneNumber;

        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return contactName;
    }


    private String parseTanggal(String date){
        DateFormat dateFormat = new SimpleDateFormat("dd MMM");
        DateFormat timeFormat = new SimpleDateFormat("kk:mm");

        Long timestamp = Long.parseLong(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        Date finaldate = calendar.getTime();

        String smsDate = dateFormat.format(finaldate)+", "+timeFormat.format(finaldate);
        return smsDate;
    }
}
