package com.example.ariodilla.smsdisplay.proses;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Fajar Cahyo Prabowo on 13-Jun-17.
 */

public class DialogBox {

    private Context context;
    private BacaTulisFile FILE = new BacaTulisFile();

    public DialogBox(Context ctx) {
        this.context = ctx;
    }

    public void inputKunci(final int cancel){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Setting No HP");
        builder.setMessage("Masukkan No HP yang digunakan untuk menerima SMS :");
        builder.setCancelable(false);

        // Set up the input
        final EditText input = new EditText(context);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_PHONE);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String kunci = input.getText().toString();
                if(kunci.length()<10){
                    Toast.makeText(context,"No telepon yang anda masukkan tidak valid",Toast.LENGTH_SHORT).show();
                    inputKunci(cancel);
                }else {
                    FILE.writeToFile(kunci,context);
                    Toast.makeText(context,"No telepon telah disimpan",Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(cancel==1){
            builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }


        builder.show();
    }
}
