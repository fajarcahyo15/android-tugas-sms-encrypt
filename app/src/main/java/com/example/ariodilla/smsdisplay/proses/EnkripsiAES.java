package com.example.ariodilla.smsdisplay.proses;

import android.util.Base64;
import android.util.Log;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


public class EnkripsiAES {


    // Method Enkripsi AES
    public static String encryptAES(String Data, String kunciEnkripsi)
            throws Exception {
        Cipher c = Cipher.getInstance("AES");
        Key key = generateKey(kunciEnkripsi);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(Data.getBytes(Charset.forName("UTF-8")));
        // String encryptedValue = new BASE64Encoder().encode(encVal); //
        // deprecated

        String encryptedValue = Base64.encodeToString(encVal, Base64.NO_PADDING);
        return encryptedValue;
    }



    // Method Dekripsi AES
    public static String decryptAES(String encryptedData, String kunciEnkripsi)
            throws Exception {
        Cipher c = Cipher.getInstance("AES");
        Key key = generateKey(kunciEnkripsi);
        c.init(Cipher.DECRYPT_MODE, key);


        byte[] decordedValue = Base64.decode(encryptedData, Base64.NO_PADDING);
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }

    private static String ambil4(String kunci){
        String potong;
        potong = kunci.substring(kunci.length()-4);
        return potong;
    }

    // Method untuk membangkitkan kunci AES dari String
    private static Key generateKey(String kunciEnkripsi) throws Exception {
        String kunci = getMD5EncryptedString(kunciEnkripsi);
        kunci = tampilPrima(kunci);
        kunci = kunci + ganjil(getMD5EncryptedString(kunciEnkripsi));
        Log.d("TAG", "Bilangan Prima : " + kunci);

        Key key = new SecretKeySpec(konversiKeByte(kunci), "AES");
        return key;
    }

    private static String tampilPrima(String md5){
        String kata = "";
        int i;
        int j;
        int bilPrima=0;

        for(i=1; i<=32; i++){
            for(j=1; j<=i/2; j++){
                if(i%j==0){
                    bilPrima++;
                }
            }

            if(bilPrima==1){
//                Log.d("TAG", "Bilangan Prima : " + i);
                kata += md5.substring(i,i+1);
            }
            bilPrima=0;
        }

        return kata;
    }

    private static String ganjil(String kunci){
        String kata = "";
        for(int i=1; i<32; i++){
            if(i%2 == 1){
                kata += kunci.substring(i,i+1);
            }
        }
        return kata;
    }

    //KUNCI = 11
    // Method untuk konversi String menjadi byte
    private static byte[] konversiKeByte(String kunci) {

        byte[] array_byte = new byte[16];
        int i = 0;

        int panjang = 0;
        if(kunci.length() > 16){
            panjang = 16;
        }

        while (i < panjang) {
            array_byte[i] = (byte) kunci.charAt(i);
            i++;
        }
        if (i < 16) {
            while (i < 16) {
                array_byte[i] = (byte) i;
                i++;
            }
        }
        return array_byte;
    }

    public static String getMD5EncryptedString(String mentah){

        String encTarget = ambil4(mentah);

        MessageDigest mdEnc = null;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception while encrypting to md5");
            e.printStackTrace();
        } // Encryption algorithm
        mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
        while ( md5.length() < 32 ) {
            md5 = "0"+md5;
        }
        return md5;
    }


}
