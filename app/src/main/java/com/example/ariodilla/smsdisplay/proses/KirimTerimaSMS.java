package com.example.ariodilla.smsdisplay.proses;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Fajar Cahyo Prabowo on 07-Jun-17.
 */

public class KirimTerimaSMS {


    public static String OperasiKiirimSMS(String pesan, String kunci, String number, final Context context) {
        String status = "";
        EnkripsiAES AES = new EnkripsiAES();
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";
        PendingIntent piSent = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
        PendingIntent piDelivered = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), 0);


        if((pesan.isEmpty())||(kunci.isEmpty()||number.isEmpty())){
            if(pesan.isEmpty()){
                status += "Kolom pesan Kosong \n";
            }

            if(kunci.isEmpty()){
                status += "Kolom kunci Kosong \n";
            }

            if(number.isEmpty()){
                status += "Kolom number Kosong \n";
            }
        }else{
            try {
                String pesanTerenkripsi = AES.encryptAES(pesan, kunci);
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(number, null, pesanTerenkripsi, piSent, piDelivered);
            } catch (Exception e) {
                status = e.toString();
                e.printStackTrace();
            }
        }

        return status;
    }
}
